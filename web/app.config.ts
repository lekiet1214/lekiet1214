export default defineAppConfig({
  socials: {
    twitter: 'https://twitter.com/lekiet0101',
    github: 'https://github.com/lekiet1214',
  },
  nuxtIcon: {
    aliases: {
      'dark-mode': 'ph:moon-bold',
      'light-mode': 'ph:sun-bold'
    }
  }
})